provider "aws" {
  region = var.region
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
}

resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_cidr
}

resource "aws_security_group" "openvpn" {
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "openvpn" {
  ami                    = "ami-0c55b159cbfafe1f0" # Amazon Linux 2 AMI
  instance_type          = var.instance_type
  key_name               = var.key_name
  subnet_id              = aws_subnet.main.id
  security_groups        = [aws_security_group.openvpn.name]
  associate_public_ip_address = true

  user_data = <<-EOF
              #!/bin/bash
              yum update -y
              yum install -y openvpn easy-rsa

              make-cadir /etc/openvpn/easy-rsa
              cd /etc/openvpn/easy-rsa
              ./easyrsa init-pki
              echo -e "\n\n\n\n\n\n\n" | ./easyrsa build-ca nopass
              ./easyrsa gen-req server nopass
              echo "yes" | ./easyrsa sign-req server server
              ./easyrsa gen-dh
              openvpn --genkey --secret /etc/openvpn/ta.key
              ./easyrsa gen-req client1 nopass
              echo "yes" | ./easyrsa sign-req client client1

              cat > /etc/openvpn/server.conf <<-EOCONF
              port 1194
              proto udp
              dev tun
              ca /etc/openvpn/easy-rsa/pki/ca.crt
              cert /etc/openvpn/easy-rsa/pki/issued/server.crt
              key /etc/openvpn/easy-rsa/pki/private/server.key
              dh /etc/openvpn/easy-rsa/pki/dh.pem
              auth SHA256
              tls-auth /etc/openvpn/ta.key 0
              cipher AES-256-CBC
              persist-key
              persist-tun
              user nobody
              group nogroup
              status /var/log/openvpn-status.log
              log /var/log/openvpn.log
              verb 3
              EOCONF

              systemctl start openvpn@server
              systemctl enable openvpn@server
              EOF
}

output "instance_public_ip" {
  value = aws_instance.openvpn.public_ip
}
